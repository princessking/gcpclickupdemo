const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 8080;
const apiKey = process.env.cuAPIKey;
const apiSecret = process.env.cuAPISecret;
const debug = 0

const request = require('request');
// app.js

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(function (req, res) {
  res.setHeader('Content-Type', 'text/plain')
  res.write('you posted:\n')
  var postedData= JSON.stringify(req.body, null, 2);
  console.log("req.body");
  console.log(req.body);
  var queryData= JSON.stringify(req.query, null, 2);
  console.log("req.query");
  console.log(req.query);
  // check for Auth Code
  for (const key in req.query) {
     if (key == "authtoken") {
	    var options = {
        url: 'https://app.clickup.com/api/v2/user',
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'X-Api-Key': apiKey,
          'Authorization': req.query[key]
        }
      }
      request(options, function(err, res, body) {
        let json = JSON.parse(body);
        console.log(json);
        });
      console.log(key, req.query[key])
    }
    if (key == "code") {
      request.post(
        'https://app.clickup.com/api/v2/oauth/token',
        {
          json: {
            client_id: apiKey,
            client_secret: apiSecret,
            code: req.query[key],
          }
        },
        (error, res, body) => {
          if (error) {
            console.error(error)
            return
          }
          console.log(`statusCode: ${res.statusCode}`)
          console.log(body)
        }
      )
      console.log(key, req.query[key])
    }
	};
  res.end(postedData + "\n QueryData: " + queryData);
})

app.get('/', function(req, res){
            //console.log(req.query.name);
            res.send('Response send to client::'+req.query.name);

});

app.listen(port, () => {

	console.log(`Example app listening on port ${port}!`)

})

